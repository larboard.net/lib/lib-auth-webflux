package net.larboard.lib.auth.security;

import lombok.NonNull;
import lombok.val;
import net.larboard.lib.auth.service.HeaderTokenAuthenticationService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;

public class HeaderTokenAuthenticationProvider implements AuthenticationProvider {

    private final HeaderTokenAuthenticationService authenticationService;

    public HeaderTokenAuthenticationProvider(@NonNull HeaderTokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public Authentication authenticate(final Authentication authentication) {
        val headerToken = (String) authentication.getCredentials();

        val user = authenticationService.authenticate(authentication);

        val authenticationResult = new AuthenticationHeaderToken(user, headerToken, user.getAuthorities());

        authenticationResult.setDetails(authentication.getDetails());

        return authenticationResult;
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return AuthenticationHeaderToken.class.isAssignableFrom(authentication);
    }
}
