package net.larboard.lib.auth.provider.google;

public interface IGoogleAuthConfig {
    String getGoogleClientId();

    String getGoogleTrustedDomainName();

    String getGoogleIssuer();
}
