package net.larboard.lib.auth.provider.jwt.keypair;

public interface IJwtKeypairAuthConfig {
    String getJwtKeyHost();

    String getJwtKeyUrl();

    String getJwtIssuer();
}
