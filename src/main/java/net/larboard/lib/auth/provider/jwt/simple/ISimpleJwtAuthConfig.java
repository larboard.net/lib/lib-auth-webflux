package net.larboard.lib.auth.provider.jwt.simple;

public interface ISimpleJwtAuthConfig {
    String getJwtSignatureSecret();

    String getJwtIssuer();
}
