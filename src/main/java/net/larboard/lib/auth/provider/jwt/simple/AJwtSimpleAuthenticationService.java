package net.larboard.lib.auth.provider.jwt.simple;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.larboard.lib.auth.service.AAuthenticationService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.userdetails.User;

@Slf4j
public abstract class AJwtSimpleAuthenticationService extends AAuthenticationService {
    @Getter
    private final ISimpleJwtAuthConfig jwtAuthConfig;

    private final byte[] jwtSignatureSecret;

    public AJwtSimpleAuthenticationService(@NonNull ISimpleJwtAuthConfig jwtAuthConfig) {
        this.jwtAuthConfig = jwtAuthConfig;

        this.jwtSignatureSecret = jwtAuthConfig.getJwtSignatureSecret().getBytes();
    }

    public User authenticateWithToken(String headerToken) {
        return checkSystemToken(headerToken);
    }

    private User checkSystemToken(String headerToken) {
        try {

            val claims = Jwts.parser()
                    .setSigningKey(jwtSignatureSecret)
                    .setAllowedClockSkewSeconds(1)
                    .parseClaimsJws(headerToken)
                    .getBody();

            return processUser(claims, headerToken);
        }
        catch (ExpiredJwtException e) {
            log.debug("auth err: {}", e.getMessage());
            throw new CredentialsExpiredException(e.getMessage(), e);
        }
        catch (JwtException e) {
            log.debug("auth err: {}", e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        }
    }

    @Override
    public String getSignerName() {
        return jwtAuthConfig.getJwtIssuer();
    }
}