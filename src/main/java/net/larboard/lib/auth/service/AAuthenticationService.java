package net.larboard.lib.auth.service;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.User;

public abstract class AAuthenticationService implements IAuthenticationService {
    protected abstract User processUser(Claims claims, String headerToken);
}
